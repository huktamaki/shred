var colors = {
  white: [255, 255, 255],
  black: [0, 0, 0]
}

var texts = {
  font: null,
  intro: {
    left: "Collective memory refers to the shared pool of memories, knowledge and information of a social group that is significantly associated with the group's identity. Collective memory can be constructed, shared, and passed on by large and small social groups.",
    right: "Advertising has brought in an advanced manner of building awareness about any product or a service in the society. It has enabled the consumers to have knowledge about the service or the product before making any purchase. Advertising has grown on the levels of creativity and innovation."
  }
}

var images = []
var amount = 400

var states = {
  text: {
    left: true,
    right: true
  },
  img: {
    left: create2DArray(13, 17, 0, true),
    right: 1
  }
}

function preload() {
  for (var i = 0; i < amount; i++) {
    images.push(loadImage('img/com' + (i + 1) + '.jpg'))
  }
  images = shuffle(images)
  texts.font = loadFont('ttf/dmmono.ttf')
}

function setup() {
  for (var i = 0; i < states.img.left.length; i++) {
    for (var j = 0; j < states.img.left[i].length; j++) {
      states.img.left[i][j] = [Math.floor(Math.random() * amount + 1), Math.floor(Math.random() * 13), Math.floor(Math.random() * 17)]
    }
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 0)
  textAlign(LEFT, TOP)
  textFont(texts.font)
  textSize(30)
  textLeading(34)
  background(colors.white)

  fill(colors.black)
  noStroke()
  rect(windowWidth * 0.5 - 232, windowHeight * 0.5, 428, 556)
  rect(windowWidth * 0.5 + 232, windowHeight * 0.5, 428, 556)

  fill(colors.white)
  noStroke()
  rect(windowWidth * 0.5 - 232, windowHeight * 0.5, 416, 544)
  rect(windowWidth * 0.5 + 232, windowHeight * 0.5, 416, 544)

  if (states.text.right !== true) {
    image(images[states.img.right], windowWidth * 0.5 + 24, windowHeight *  0.5 - 272)
  } else {
    fill(colors.black)
    noStroke()
    text(texts.intro.right, windowWidth * 0.5 + 240, windowHeight * 0.5, 416, 544)
  }

  if (states.text.left !== true) {
    for (var i = 0; i < states.img.left.length; i++) {
      for (var j = 0; j < states.img.left[i].length; j++) {
        image(images[states.img.left[i][j][0] - 1].get(states.img.left[i][j][1] * 32, states.img.left[i][j][2] * 32, 32, 32), windowWidth * 0.5 - 440 + i * 32, windowHeight * 0.5 - 272 + j * 32)
      }
    }
    if (mouseX < windowWidth * 0.5 - 32 && mouseX > windowWidth * 0.5 - 448 && mouseY < windowHeight * 0.5 + 272 && mouseY > windowHeight * 0.5 - 272) {
      states.img.right = states.img.left[Math.floor(13 - (windowWidth * 0.5 - 32 - mouseX) / 32)][Math.floor(17 - (windowHeight * 0.5 + 272 - mouseY) / 32)][0] - 1
    }
  } else {
    fill(colors.black)
    noStroke()
    text(texts.intro.left, windowWidth * 0.5 - 220, windowHeight * 0.5, 416, 544)
  }
}

function mousePressed() {
  if (states.text.left !== true) {
    if (mouseX < windowWidth * 0.5 - 32 && mouseX > windowWidth * 0.5 - 448 && mouseY < windowHeight * 0.5 + 272 && mouseY > windowHeight * 0.5 - 272) {
      states.img.left[Math.floor(13 - (windowWidth * 0.5 - 32 - mouseX) / 32)][Math.floor(17 - (windowHeight * 0.5 + 272 - mouseY) / 32)][0] = Math.floor(Math.random() * amount + 1)
      states.img.left[Math.floor(13 - (windowWidth * 0.5 - 32 - mouseX) / 32)][Math.floor(17 - (windowHeight * 0.5 + 272 - mouseY) / 32)][1] = Math.floor(Math.random() * 13)
      states.img.left[Math.floor(13 - (windowWidth * 0.5 - 32 - mouseX) / 32)][Math.floor(17 - (windowHeight * 0.5 + 272 - mouseY) / 32)][2] = Math.floor(Math.random() * 17)
    }
  } else {
    if (mouseX < windowWidth * 0.5 - 32 && mouseX > windowWidth * 0.5 - 448 && mouseY < windowHeight * 0.5 + 272 && mouseY > windowHeight * 0.5 - 272) {
      setTimeout(function() {
        states.text.left = false
      }, 1)
    }
  }
  if (states.text.right !== true) {
    if (mouseX > windowWidth * 0.5 + 32 && mouseX < windowWidth * 0.5 + 448 && mouseY < windowHeight * 0.5 + 272 && mouseY > windowHeight * 0.5 - 272) {
      for (var i = 0; i < states.img.left.length; i++) {
        for (var j = 0; j < states.img.left[i].length; j++) {
          states.img.left[i][j] = [Math.floor(Math.random() * amount + 1), Math.floor(Math.random() * 13), Math.floor(Math.random() * 17)]
        }
      }
      images = shuffle(images)
    }
  } else {
    if (mouseX > windowWidth * 0.5 + 32 && mouseX < windowWidth * 0.5 + 448 && mouseY < windowHeight * 0.5 + 272 && mouseY > windowHeight * 0.5 - 272) {
      setTimeout(function() {
        states.text.right = false
      }, 1)
    }
  }
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}

function windowResized() {
  createCanvas(windowWidth, windowHeight)
}
